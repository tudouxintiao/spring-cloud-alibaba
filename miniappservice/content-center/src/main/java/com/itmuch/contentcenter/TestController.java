package com.itmuch.contentcenter;

import com.itmuch.contentcenter.dao.content.ShareMapper;
import com.itmuch.contentcenter.domain.dto.content.ShareDTO;
import com.itmuch.contentcenter.domain.dto.user.UserDTO;
import com.itmuch.contentcenter.domain.entity.content.Share;
import com.itmuch.contentcenter.feignclient.UserCenterFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * Created with IntelliJ IDEA.
 * User: Administrator
 * Date: 2019/8/31 0031
 * Time: 11:39
 */
@RestController
@Slf4j
public class TestController {

//    @Autowired
//    private RestTemplate restTemplate;

    @Autowired
    private UserCenterFeignClient feignClient;

    @Autowired
    private ShareMapper shareMapper;

    @GetMapping("/test")
    public List<Share> testInsert() {
        Share share = new Share();
        share.setAuthor("wjs");
        share.setBuyCount(555);
        share.setCreateTime(new Date());
        share.setUpdateTime(new Date());
        share.setPrice(666);
        shareMapper.insertSelective(share);
        return shareMapper.selectAll();
    }

    @GetMapping("/findShare/{id}")
    public ShareDTO find(@PathVariable Integer id) {
        Share share = shareMapper.selectByPrimaryKey(id);
//        List<ServiceInstance> userCenterServices = discoveryClient.getInstances("user-center");
//       List<String> targetURLS = userCenterServices.stream()
//               .map(instance->instance.getUri().toString()+"/users/{id}")
//               .collect(Collectors.toList());
//        Integer i= ThreadLocalRandom.current().nextInt(targetURLS.size());
//        String targetUrl= targetURLS.get(i);
//        log.info("请求地址：{}",targetUrl);
//        UserDTO userDTO = restTemplate.getForObject("http://user-center/users/{id}", UserDTO.class, share.getUserId());
       UserDTO userDTO =feignClient.findById(share.getUserId());
        ShareDTO shareDTO = new ShareDTO();
        BeanUtils.copyProperties(share, shareDTO);
        shareDTO.setWxNickName(userDTO.getWxNickname());
        return shareDTO;
    }

    @Autowired
    private DiscoveryClient discoveryClient;

    @GetMapping("/test2")
    public List<ServiceInstance> test2() {
        return discoveryClient.getInstances("user-center");
    }
}
